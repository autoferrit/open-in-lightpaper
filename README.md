##### Note - I am not easily to maintain this much anymore (not that there is much to maintain) as I no longer have a Mac. But if anyone is interested in doing anything with it, let me know.

# Open into LightPaper Markdown Editor

Open your current *.md files into the Markdown Viewer LightPaper.<br>
You need to install the app 'LightPaper'.<br>
Visit this site.<br>
http://clockworkengine.com/lightpaper-mac/

Simply enter in the command <kbd>alt</kbd>+<kbd>meta</kbd>+<kbd>M</kbd> top
open the current
file into
lightpaper.

All credit should go to the open in marked plugin written by @vexus2.
This project was cloned from there and simply updated to work with my preferred
markdown editor.

https://github.com/vexus2/open-in-marked
